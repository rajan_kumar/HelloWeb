
package com.amdocs.webapp;
import static org.junit.Assert.*;
import java.io.*;
import javax.servlet.http.*;
import org.junit.Test;
import org.mockito.Mockito;

public class HelloWebAppTest extends Mockito{

    @Test
    public void testServlet() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);       
        HttpServletResponse response = mock(HttpServletResponse.class);    


        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);
        when(response.getWriter()).thenReturn(writer);

        new HelloWebApp().doGet(request, response);
        
        writer.flush(); // it may not have been flushed yet...
        System.out.print(stringWriter.toString());
        assertTrue("Expecting Hello ECOMP and CANOPI Team but not found",stringWriter.toString().contains("Hello ECOMP and CANOPI Team"));
    }

    @Test
    public void testAdd() throws Exception {

        int k= new HelloWebApp().add(7,5);
        assertEquals("Problem with Add function:", 12, k);
        
    }
  @Test 
    public void testSub() throws Exception {

        int k= new HelloWebApp().sub(8,7);
        assertEquals("Problem with Sub function:", 1, k);

    }
}
